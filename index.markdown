---
layout: default
title: File directory
---


<div class="default">

<h2 class="post-list-heading">Board meeting minutes </h2>
<ul>
{% for mf in site.static_files %}
{% if mf.path contains 'mom_files/' %}
<li>
    <a href="{{site.baseurl}}{{mf.path}}">{{ mf.name }}</a>
</li>
{% endif %}
{% endfor %}
</ul>
<h2 class="post-list-heading">Other files </h2>
<ul>
{% for mf in site.static_files %}
{% if mf.path contains 'other_files/' %}
<li>
    <a href="{{site.baseurl}}{{mf.path}}">{{ mf.name }}</a>
</li>
{% endif %}
{% endfor %}
</ul>


</div>
